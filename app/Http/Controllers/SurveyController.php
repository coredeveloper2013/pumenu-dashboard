<?php

namespace App\Http\Controllers;

use App\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
	public function list()
	{
		$surveys = Survey::where('user_id', Auth()->id())->get();
		return view('admin.survey.survey-list', compact('surveys'));
	}
	public function survey_approve(Request $request)
	{
		$survey = Survey::find($request->id);
		if ($survey->status == 1) {
			$status = 0;
		}else {
			$status = 1;
		}
		$survey->status = $status;
		$survey->save();
		return redirect(action('SurveyController@list'));
	}
}

