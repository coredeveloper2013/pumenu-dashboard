<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Log;
use App\Rating;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;

class DashboardController extends Controller{
  
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(){
    if(Auth::check() && Auth::user()->role == 1){
      $exp = DB::select('select sum(amount) as c from expense');
      
      $sales = DB::select('select sum(total) as c  from pos');
      
      $user = DB::select('select count(id) as c from users where role = 2');
      
      $profit = str_replace('-', '', ($exp[ 0 ]->c-$sales[ 0 ]->c));
      
      $visits = DB::select('select sum(id) as c from log');
      
      $dish = DB::select('select sum(id) as c from dishes');
      
      $categories = DB::select('select sum(id) as c from categories');
      
      $drinks = DB::select('select sum(id) as c from drinks');
      
      $specialM = DB::select('select sum(id) as c from special_menu');
      
      $tH = DB::select('select * from users ');
      $first_user_id = User::first()->id;
      $chart = [];
      
      /** getting all hotels  **/
      
      $hotelId = (strpos(url()->full(), '?id=') !== FALSE) ? @end(explode('?id=', url()->full())) : $first_user_id;
      $hotels = DB::select("select * from users where ".((is_numeric($hotelId)) ? "id = ".$hotelId : "id > 0")." limit 1 ");
      
      if(count($hotels) > 0){
        
        foreach($hotels as $h){
          
          $chart[ str_replace(' ', '_', $h->name) ][ 'user' ] = $h;
          /*getting categories*/
          $chart[ str_replace(' ', '_', $h->name) ][ 'special_menu' ] = DB::select("select * from special_menu  where user_id = ".$h->id);
          
          $chart[ str_replace(' ', '_', $h->name) ][ 'settings' ] = DB::select("select * from settings  where user_id = ".$h->id);
          
          $chart[ str_replace(' ', '_', $h->name) ][ 'pos' ] = DB::select("select * from pos  where user_id = ".$h->id);
          
          $chart[ str_replace(' ', '_', $h->name) ][ 'categories' ] = DB::select("select * from categories  where user_id = ".$h->id);
          
          /* category wise data */
          $counter = 30;
          for($i = 1 ; $i <= (cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'))) ; $i++){
            /*date*/
            $dbDate = Carbon::now()->subDays($counter--);
            $dateDisplay = @reset(explode(' ', $dbDate));
            
            $chart[ str_replace(' ', '_', $h->name) ][ 'visitors' ][ $dateDisplay ] = DB::select("select count(id) as c from log where user_id = ".$h->id." and created_at like '%".$dateDisplay."%'")[ 0 ]->c;
            
            $c = DB::select("select count(url) as c , url from log where user_id = ".$h->id."  and created_at like '%".$dateDisplay."%' GROUP BY url");
            
            $chart[ str_replace(' ', '_', $h->name) ][ 'url' ][ $dateDisplay ] = (isset($c->c)) ? $c->c.' - '.$c->url : '';
          }
          
          $rank = (array) DB::select("select count(id) as rank , user_id from log GROUP by user_id order by rank desc");
          
          foreach($rank as $k => $r) if($r->user_id == $h->id) $rank = $k+1;
          
          $totalVisits = DB::select("SELECT count(url) as v FROM `log` where user_id = $h->id");
          
          $todayHits = DB::select("select count(url) as c  from log where user_id = ".$h->id."  and created_at like '%".date('Y-m-'.((date('d') < 10) ? '0'.date('d') : date('d')))."%'");

          $qrScannedUrl = public_url(urlencode($h->name) . '/?qrCode');
          $qrScanned = DB::select("select count(url) as c  from log where url like '%".$qrScannedUrl ."%' OR url like '%". $qrScannedUrl ."%'");

          $mostVisited = DB::select("select count(id) as c , url from log where user_id = '".$h->id."' GROUP by url  order by c desc");
        }
      }
    }else{
      /*user specific search*/
      $exp = DB::select("select sum(amount) as c from expense where `user_id` = ".Auth::user()->id);
      $sales = DB::select("select sum(total) as c  from pos where `user_id` = ".Auth::user()->id);
      $profit = str_replace('-', '', ($exp[ 0 ]->c-$sales[ 0 ]->c));
      $visits = DB::select("select sum(id) as c from log where `user_id` = ".Auth::user()->id);
      $dish = DB::select("select sum(id) as c from dishes where `user_id` = ".Auth::user()->id);
      $categories = DB::select("select sum(id) as c from categories where `user_id` = ".Auth::user()->id);
      $drinks = DB::select("select sum(id) as c from drinks where `user_id` = ".Auth::user()->id);
      $specialM = DB::select("select sum(id) as c from special_menu where `user_id` = ".Auth::user()->id);
      $chart = [];
      
      /** getting all hotels  **/
      
      $hotels = DB::select("select * from users where id  = '".Auth::user()->id."' limit 1 ");
      
      if(count($hotels) > 0){
        
        foreach($hotels as $h){
          
          $chart[ str_replace(' ', '_', $h->name) ][ 'user' ] = $h;
          /*getting categories*/
          $chart[ str_replace(' ', '_', $h->name) ][ 'special_menu' ] = DB::select("select * from special_menu  where user_id = ".$h->id);
          
          $chart[ str_replace(' ', '_', $h->name) ][ 'settings' ] = DB::select("select * from settings  where user_id = ".$h->id);
          
          $chart[ str_replace(' ', '_', $h->name) ][ 'pos' ] = DB::select("select * from pos  where user_id = ".$h->id);
          
          $chart[ str_replace(' ', '_', $h->name) ][ 'categories' ] = DB::select("select * from categories  where user_id = ".$h->id);
          
          /* category wise data */
          $counter = 30;
          for($i = 1 ; $i <= 30 ; $i++){
            $dbDate = Carbon::now()->subDays($counter--);
            $dateDisplay = @reset(explode(' ', $dbDate));
            
            $chart[ str_replace(' ', '_', $h->name) ][ 'visitors' ][ $dateDisplay ] = DB::select("select count(id) as c from log where user_id = ".$h->id." and created_at like '%".$dateDisplay."%'")[ 0 ]->c;
            
            $c = DB::select("select count(url) as c , url from log where user_id = ".$h->id."  and created_at like '%".$dateDisplay."%' GROUP BY url");
            
            $chart[ str_replace(' ', '_', $h->name) ][ 'url' ][ $dateDisplay ] = (isset($c->c)) ? $c->c.' - '.$c->url : '';
          }
          
          $rank = (array) DB::select("select count(id) as rank , user_id from log GROUP by user_id order by rank desc");
          
          foreach($rank as $k => $r) if($r->user_id == $h->id) $rank = $k+1;
          
          $totalVisits = DB::select("SELECT count(url) as v FROM `log` where user_id = $h->id");
          
          $todayHits = DB::select("select count(url) as c  from log where user_id = ".$h->id."  and created_at like '%".date('Y-m-'.((date('d') < 10) ? '0'.date('d') : date('d')))."%'");
          
          $qrScannedUrl = public_url(urlencode($h->name) . '/?qrCode');
          $qrScanned = DB::select("select count(url) as c  from log where url like '%".$qrScannedUrl ."%' OR url like '%". $qrScannedUrl ."%'");

          
          $mostVisited = DB::select("select count(id) as c , url from log where user_id = '".$h->id."' GROUP by url  order by c desc");
        }
      }
    }
    
    
    $desk = Log::where('cat', 'd')->where('user_id', ((isset($hotelId)) ? $hotelId : Auth::user()->id ))->get()->toArray();
    $phone = Log::where('cat', 'p')->where('user_id', ((isset($hotelId)) ? $hotelId : Auth::user()->id ))->get()->toArray();
    
    $trendingDishesOfMonth = Log::where('created_at' , '>' , Carbon::today()->subDays(30)->format('Y-m-d H:i:s'))->where('user_id', ((isset($hotelId)) ? $hotelId : Auth::user()->id ))->whereRaw("  `url` like '%/menu/%' ")->get(['id' , 'url'])->toArray();
    $trendingDishesOfAllTime = Log::where('user_id', ((isset($hotelId)) ? $hotelId : Auth::user()->id ))->whereRaw("  `url` like '%/menu/%' ")->get(['id' , 'url'])->toArray();
      
    $hotel_id = isset($hotelId) ? $hotelId : Auth::user()->id ;
    /**
     * RATING
     */
    // $ratings = Rating::where('user_id', $hotel_id)->get()->groupBy('rating');
    $ratings = DB::select("select count(id) as c, rating from ratings where user_id=$hotel_id group by `rating` order by rating");
    $this_month = date('n');
    $ratings_current_months = DB::select("select count(id) as c, rating from ratings where (user_id=$hotel_id and MONTH(created_at) = $this_month  ) group by `rating` order by rating");


    /**
     * Source
     */


    // coming from shortcode
    $coming_by_logs = $this->coming_by_logs( urlencode($h->name));
    $coming_by_logs_current_month = $this->coming_by_logs( urlencode($h->name), true);
    // return [$coming_by_logs, $coming_by_logs_current_month];

    return view('admin.dashboard',
      compact(
        'exp', 'sales', 'profit', 'visits', 'dish', 'categories', 'drinks', 'specialM', 'user', 'chart', 'hotels', 'tH', 'rank', 'totalVisits', 'todayHits',
        'qrScanned', 'mostVisited', 'desk', 'phone' , 'trendingDishesOfMonth' , 'trendingDishesOfAllTime', 'ratings', 'ratings_current_months',
        'coming_by_logs', 'coming_by_logs_current_month'
      )
    );
  }
  public function coming_by_logs($restaurant_name, $isMonth=null) {
    //matching url 
    $qrScannedUrl = public_url($restaurant_name . '/?qrCode');
    $qrScannedUrl2 = public_url($restaurant_name . '?qrCode');
    $cb_shortcode = public_url($restaurant_name . '?comingByShortcode');
    $cb_name = public_url($restaurant_name . '?comingByName');
    $cb_nearby = public_url($restaurant_name . '?comingByNearby');
    $cb_direct_url = public_url($restaurant_name);

    // getting log
    $qr_log = DB::select("select count(url) as traffic  from log where url like '%{$qrScannedUrl}%' OR url like '%{$qrScannedUrl2}%'" );
    $shortcode_log = DB::select("select count(url) as traffic from log where url like '%{$cb_shortcode}%' " );
    $name_log = DB::select("select count(url) as traffic  from log where url like '%{$cb_name}%' " );
    $nearby_log = DB::select("select count(url) as traffic  from log where url like '%{$cb_nearby}%' " );
      // direct url log will have % in the beginning only
    $direct_url_log = DB::select("select count(url) as traffic  from log where url like '%{$cb_direct_url}' " );
    if ($isMonth) {
      // getting log
      $this_month =  date('n');
      $monthQuery = "and MONTH(created_at) = $this_month";
      $qr_log = DB::select("select count(url) as traffic  from log where url like '%{$qrScannedUrl}%' OR url like '%{$qrScannedUrl2}%' $monthQuery " );
      $shortcode_log = DB::select("select count(url) as traffic from log where url like '%{$cb_shortcode}%' $monthQuery" );
      $name_log = DB::select("select count(url) as traffic  from log where url like '%{$cb_name}%' $monthQuery " );
      $nearby_log = DB::select("select count(url) as traffic  from log where url like '%{$cb_nearby}%' $monthQuery " );
        // direct url log will have % in the beginning only
      $direct_url_log = DB::select("select count(url) as traffic  from log where url like '%{$cb_direct_url}' $monthQuery " );
    }

    $qr_item = count($qr_log) ? $qr_log[0]->traffic : 0; 
    $shortcode_item = count($shortcode_log) ? $shortcode_log[0]->traffic : 0; 
    $name_item = count($name_log) ? $name_log[0]->traffic : 0; 
    $nearby_item = count($nearby_log) ? $nearby_log[0]->traffic : 0; 
    $direct_uri_item = count($direct_url_log) ? $direct_url_log[0]->traffic : 0; 
    return [
      'Traffic by QR' => $qr_item,
      'Traffic by short code' => $shortcode_item,
      'Traffic by name' => $name_item,
      'Traffic by nearby location' => $nearby_item,
      'Traffic by direct url' => $direct_uri_item,
    ];
  }
  
  public function imagedownload(Request $request){
    
    $url = base64_decode($request->get('img'));
    
    $name = 'QrScanner'.@end(explode('.', $url));
    
    $image = Image::make($url)->encode('jpg');
    $headers = ['Content-Type'        => 'image/jpeg',
                'Content-Disposition' => 'attachment; filename='.$name,];
    return response()->stream(function() use ($image){
      echo $image;
    }, 200, $headers);
  }
  
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(){
    //
  }
  
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request){
    //
  }
  
  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id){
    //
  }
  
  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id){
    //
  }
  
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int                      $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id){
    //
  }
  
  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id){
    //
  }
}
