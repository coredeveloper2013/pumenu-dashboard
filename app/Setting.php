<?php

namespace App;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model{
  use HasSlug;
  protected $guarded = [];

  public function user(){
    return $this->belongsTo('App\User', 'user_id', 'id');
  }
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('shortcode');
    }
}
