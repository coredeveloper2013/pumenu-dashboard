<?php

use App\Rating;
use Faker\Generator as Faker;

Rating::truncate();

$factory->define(Rating::class, function (Faker $faker) {
		$ip_address = '192.'. rand(1, 255) .'.'. rand(1, 255) .'.' . rand(1, 255);
		$user_id = rand(1, 2);
		$rating = rand(1, 5);
    return [
    	'ip_address' => $ip_address,
    	'user_id' => $user_id,
    	'rating' => $rating,
    ];
});

echo 'Rating Seeding successful';