# working data
=================
~~~js
?qrCode
?comingByName
?comingByShortcode
?comingByNearby
direct

~~~
# to get app address using `request()` or `Request::`
~~~php
request()->root();
Request::fullUrl(),
request()->fullUrl(),
~~~

# getting this month results 
~~~php
$this_month = date('n');
$ratings_last_month = DB::select("select count(id) as c, rating from ratings where (user_id=$hotel_id and MONTH(created_at) = $this_month  ) group by `rating` order by rating");
~~~

# making google column chart  

~~~html
<div class="col-lg-6 pull-left grpahinformation">
  <div id="restaurant_rating"></div>
</div>
~~~


# for column chart
~~~js
google.charts.setOnLoadCallback(restaurant_rating);
function restaurant_rating() {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string' , 'Topping');
		data.addColumn('number' , 'Count');
		data.addColumn({ type: 'string', role: 'style' });
		var colors=['salmon', 'teal', 'brown', 'cadetblue', 'burlywood'];
		data.addRows([<?php if(isset($ratings)){
      foreach($ratings as $rating){ ?>
		['Rating <?= $rating->rating ?>' , <?= $rating->c ?>, colors.shift()] ,
      <?php }
      }?>
		]);
		var options = {
				'title' : 'Restaurant Rating' ,
				'width' : '100%',
				'height' : 400,
				vAxis: {
          minValue: 0,
        },

		};
		var chart = new google.visualization.ColumnChart(document.getElementById('restaurant_rating'));
		chart.draw(data , options);
}
~~~

~~~php
Array
(
    [0] => stdClass Object
        (
            [c] => 18
            [rating] => 1
        )

    [1] => stdClass Object
        (
            [c] => 21
            [rating] => 2
        )

    [2] => stdClass Object
        (
            [c] => 21
            [rating] => 3
        )

    [3] => stdClass Object
        (
            [c] => 18
            [rating] => 4
        )

    [4] => stdClass Object
        (
            [c] => 18
            [rating] => 5
        )

)
~~~

# for pie chart


~~~js
function restaurant_rating2() {
		// Create the data table.
		var data = new google.visualization.DataTable();
		data.addColumn('string' , 'Topping');
		data.addColumn('number' , 'Slices');
		var colors=['salmon', 'teal', 'brown', 'cadetblue', 'burlywood'];
		data.addRows([<?php if(isset($ratings)){
      foreach($ratings as $rating){ ?>
		['Rating <?= $rating->rating ?>' , <?= $rating->c ?>] ,
      <?php }
      }?>
		]);
		var options = {
				'title' : 'Restaurant Rating' ,
				'width' : '100%',
				'height' : 400
		};
		var chart = new google.visualization.PieChart(document.getElementById('restaurant_rating'));
		chart.draw(data , options);
}
~~~

