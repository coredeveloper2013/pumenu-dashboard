===================== Total (minimum) =============== 62H

# 1or8 - like modules - 12:00 H
1. database migration - 30:00 
2. adding rate us and popup for rating option (front end view) - 3:00
3. making ajax like and show results and handle already rated ip, getting from unique ip for specific restaurant - 4:00
5. Showing graph based upon daily rating - 4:30+


# 2 Header Opacity features:  4:00 H
1. dashboard option - database migration - 0:30 H
2. adding option in form for color picker and opacity picker in view and adding to database - 2:30 H
3. implementation opacity in front view - 1:00 H

# 4 - Prev and next category features: 8:00 H
1. adding next and previous category link in single food menu page - 4:00 H - it requires lot of calculation due to existing schema style
2. adding swipe event for mobile to change category - 4:00 H

# 4 - placing chevron button : 2:00 H
1. adding chevron left and right button, adding back link based on English and Hebrew language - 2:00 H

# 5 - translation for category 3:00 H
1. database migration for new column in database - 0:30 H
2. adding CRUD operation for new column - 2:00 H
3. adding translated option for category in front view - 0:30 H

# 6.1. Nearby restaurant features 10:00 H ++
1. coding for getting user lat long using geolocation api - 2:00 H + RND
2. cheeking 2km radius and evaluate from database and made json response - 4:00 H + RND
3. adding facebook like cheek-in hinting in front view for available restaurant  - 4:00 H

# 6.2 directly use qr code redirection - not possible 
It can't be implement. But it's doable. Its require lot of effort but end result won't be satisfactory. Client side HTML5 App will not read the qr picture properly. This is because the picture from the QR code will be sent to the client server and it will not be correct most of the time.

# 6.3 redirect restaurant page using shortcode like `xyz` - 7:00 H
1. adding database column for that  0:30 H
2. adding CRUD for shortcode(it will be unique) 3:00 H
3. implement features in front view 4:00 H (since I have to intact earlier behavior)

# 7 tracking - from where user redirecting to restaurant page - 8:00 H
1. tracking - from where user redirecting to restaurant page - 3:00 H
2. Showing graph based on user redirection  in admin panel - 5:00 H

# 9 menu image lightbox and previous and next 8:00 H
1. showing lightbox for image with and keep group all images from same category - 4:00 H
2. sending next category and showing lightbox by default (it requires redirection source) - 4:00H 








