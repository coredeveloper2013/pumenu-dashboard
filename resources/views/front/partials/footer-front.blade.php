<!--content end-->
<!--=============== footer ===============-->
<?php
$allowed = ['catmenu', 'menu'];
if(in_array(@end(explode('@', request()->route()->getAction('controller'))), $allowed)){
?>
<footer>
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <!--tiwtter-->
        <div class="col-md-4 hidden">
          <div class="footer-info">
            <div class="twitter-holder">
              <div class="twitts">
                <div class="twitter-feed">
                  <div id="twitter-feed"></div>
                </div>
              </div>
              <div class="customNavigation">
                <a class="prev-slide transition"><i class="fa fa-long-arrow-left"></i></a>
                <a class="twit-link transition" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                <a class="next-slide transition"><i class="fa fa-long-arrow-right"></i></a>
              </div>
            </div>
          </div>
        </div>
        <!--footer social links-->
        <div class="col-md-12 footerlinker">
          <div class="footer-social">
            <h3>Find us</h3>
            <ul>
              <li><a href="<?= (isset($setting->facebook) && !empty($setting->facebook)) ? $setting->facebook : '#'; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li class="hidden"><a href="<?= (isset($setting->twitter) && !empty($setting->twitter)) ? $setting->twitter : '#'; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="<?= (isset($setting->instagram) && !empty($setting->instagram)) ? $setting->instagram : '#'; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
              <li class="hidden"><a href="<?= (isset($setting->pintrest) && !empty($setting->pintrest)) ? $setting->pintrest : '#'; ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
              <li class="hidden"><a href="<?= (isset($setting->tumbler) && !empty($setting->tumbler)) ? $setting->tumbler : '#'; ?>" target="_blank"><i class="fa fa-tumblr"></i></a></li>
            </ul>
          </div>
        </div>
        <!--subscribe form-->
        <div class="col-md-4 hidden">
          <div class="footer-info">
            <h4>Newsletter</h4>
            <div class="subcribe-form">
              <form id="subscribe">
                @csrf
                <input class="enteremail" name="email" id="subscribe-email" placeholder="Your email address.." spellcheck="false" type="text">
                <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fa fa-envelope"></i></button>
                <label for="subscribe-email" class="subscribe-message"></label>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="bold-separator">
        <span></span>
      </div>
      <!--footer contacts links -->
      {{--<ul class="footer-contacts">
        <li><a href="#">+7(111)123456789</a></li>
        <li><a href="#">27th Brooklyn New York, NY 10065</a></li>
        <li><a href="#">yourmail@domain.com</a></li>
      </ul>--}}
    </div>
  </div>
  <!--to top / privacy policy-->
  <div class="to-top-holder">
    <div class="container">
      <p><span> &#169; {{ date('Y') }} . </span> All rights reserved.</p>
      <div class="to-top"><span>Back To Top </span><i class="fa fa-angle-double-up"></i></div>
    </div>
  </div>
</footer>
<?php } ?>
<!--footer end -->
</div>
<!-- wrapper end -->
</div>
<!-- Main end -->
<!--=============== google map ===============-->

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyD5H5DwCVRwToK6BAHjjor3qNj1Taj5JUc"></script>

<!--=============== scripts  ===============-->
<script type="text/javascript" src="{{ asset('front/js/jquery.min.js') }}"></script>
<script type="text/javascript" src={{ asset('front/js/plugins.js') }}></script>
<script type="text/javascript" src={{ asset('front/js/scripts.js') }}></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/css/iziToast.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/js/iziToast.min.js"></script>

<script>
		setInterval(function () {
				$('html').find('.next-slide.transition').trigger('click');
		} , 3000);
		$(".language-changer  a").on('click' , function (event) {
				
				if ($(this).attr('href') == '#') {
						event.preventDefault();
						var lang = $(this).attr('class');
						if (lang == 'en') {
								$("[data-langen]").each(function (index , val) {
										$(this).html($(this).attr('data-langen'));
								});
						} else {
								$("[data-langhe]").each(function (index , val) {
										$(this).html($(this).attr('data-langhe'));
								});
						}
				}
		});
		/*form serialize and */
		$.ajax({
				url : '' ,
				type : 'psot' ,
				data : $('#subscribe').serialize() ,
				dataType : 'json' ,
				success : function (data) {
						console.log(data);
				} ,
				error : function (request , error) {
						console.log("Request: " + JSON.stringify(request));
				}
		});

</script>
</body>
</html>