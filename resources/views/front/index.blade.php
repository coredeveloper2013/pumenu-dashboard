@extends('front.partials.mainLayout-front')
@section('content')
  <style>
    #main > header { display: none; }

   
  </style>
  <a href="<?= url('/') ?>"> 
    <img src="{{ asset("images/popup_menu_final_white-01.png") }}" alt="Logo" class="image-responsive sliderLogo" style="max-width: 250px; padding: 35px; margin-top: 18px;">
  </a>

  <!--=============== Hero content ===============-->
  <div class="content full-height hero-content">
    <div class="fullheight-carousel-holder" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);">
      <div class="customNavigation">
        <a class="prev-slide transition"><i class="fa fa-long-arrow-left"></i></a>
        <a class="next-slide transition"><i class="fa fa-long-arrow-right"></i></a>
      </div>
      <?php if(isset($slider) && count($slider) > 0){ ?>
      <div class="fullheight-carousel owl-carousel">
        <!--=============== 1 ===============-->
        <?php foreach($slider as $s){ ?>
        <div class="item full-height">
          <div class="carousel-item">
            <div class="overlay"></div>
            <div class="bg" style="background-image: url({{ asset('storage/uploads/'.$s->image) }})"></div>
            <div class="carousel-link-holder">
              <h3>
                <a style="display: none" href="{{public_url('/menu/'. request()->route('id') . '/cat/' . $s->id)}}" ><?= $s->title ?></a>
                <a href="<?= action('frontEndController@menu', ['id' => request()->route('id')]).'/'.$s->title  ?>"><?= $s->title ?></a>
              </h3>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php
      }else{ ?>
      <h2>Sorry this Resturent does not have Categories</h2>
      <?php } ?>
    </div>
  </div>
  {{-- <div class="item" style="position: absolute; width: 300px; left: 50%;margin-left: -150px;z-index: 999; bottom: 0; display: none">
     --}}{{--<img src="{{ asset('storage/qr/'.'hotel-'.request()->route('id').'.png') }}" class="respimg" alt="Please regenerate Qr code">--}}{{--
   </div>--}}
@endsection()