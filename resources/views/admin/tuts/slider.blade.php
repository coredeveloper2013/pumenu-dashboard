<p>
	<small style="margin-top: 10px; font-size: 14px;">
		Note : About category section <br>
		Category represents your menu category. You must include image for your category. Image height should be more than 1000px. Width should be more than 700px. I mean large portrait mode image. 
		Once you set category and associate dishes, drinks and special menu with category, it will show in landing page slider with background image. <br>    
		You can associate category with Dishes, Drinks and Special menu section. 
	</small>
</p>