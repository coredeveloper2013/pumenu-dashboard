<p>
	<small style="margin-top: 10px; font-size: 14px;">
		Note : About Dishes section <br>
		Dish will show under specific category in your restaurant landing page. <br>
		You can add, edit, delete your restaurant dish item. You need to associated categories with your dish. Visitor will be able to see your dish under a category. 
	</small>
</p>
